import {SAVE_ITEM} from "./constants";

export const saveItem = (item) => {
    return {
        type: SAVE_ITEM,
        payload: item
    }
};