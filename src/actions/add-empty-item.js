import {ADD_EMPTY_ITEM} from "./constants";

export const addEmptyItem = () => {
    return {
        type: ADD_EMPTY_ITEM
    }
};