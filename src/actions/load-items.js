import {GET_ALL} from "./constants";

export const loadItems = (items) => {
    return {
        type: GET_ALL,
        payload: items
    }
};