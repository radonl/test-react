import {REMOVE_ITEM} from "./constants";

export const removeItem = (item) => {
    return {
        type: REMOVE_ITEM,
        payload: item
    }
};