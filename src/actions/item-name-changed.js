import {ITEM_NAME_CHANGED} from "./constants";

export const itemNameChanged = (index, name) => {
    return {
        type: ITEM_NAME_CHANGED,
        payload: {index, name}
    }
};