export interface EntityId {
    id: string;
}

export interface CreatedResponse {
    identifiers: EntityId[];
    generatedMaps: EntityId[];
    raw: EntityId[]
}
