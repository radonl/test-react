import ListService from "../services/list.service";
import Api from '../services/api';

jest.mock('../services/api');

const items = [{
    id: 'id',
    name: 'name',
    index: 0
}];

const error = {
    response: 'error'
};

describe('list service', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should successfully get list', async () => {
        jest.spyOn(Api, 'get')
            .mockImplementation((url) => Promise.resolve({items,url}));
        const res = await ListService.getList();
        expect(res).toEqual({
            items,
            url: 'list'
        });
    });

    it('should unsuccessfully get list', async () => {
        jest.spyOn(Api, 'get')
            .mockImplementation((url) => Promise.reject(error));
        try {
            await ListService.getList();
        } catch (e) {
            expect(e).toEqual('error');
        }
    });

    it('should successfully create item', async () => {
        jest.spyOn(Api, 'post')
            .mockImplementation((url, body) => Promise.resolve({url, body}));
        const res = await ListService.createItem(items[0]);
        expect(res).toEqual({
            url: 'list',
            body: {
                name: 'name'
            }
        });
    });

    it('should unsuccessfully create item', async () => {
        jest.spyOn(Api, 'post')
            .mockImplementation((url) => Promise.reject(error));
        try {
            await ListService.createItem(items[0]);
        } catch (e) {
            expect(e).toEqual('error');
        }
    });

    it('should successfully update item', async () => {
        jest.spyOn(Api, 'put')
            .mockImplementation((url, body) => Promise.resolve({url, body}));
        const res = await ListService.updateItem(items[0]);
        expect(res).toEqual({
            url: 'list',
            body: {
                name: 'name',
                id: 'id'
            }
        });
    });

    it('should unsuccessfully update item', async () => {
        jest.spyOn(Api, 'put')
            .mockImplementation((url) => Promise.reject(error));
        try {
            await ListService.updateItem(items[0]);
        } catch (e) {
            expect(e).toEqual('error');
        }
    });

    it('should successfully remove item', async () => {
        jest.spyOn(Api, 'delete')
            .mockImplementation((url) => Promise.resolve({url}));
        const res = await ListService.removeItem(items[0]);
        expect(res).toEqual({
            url: `list/${items[0].id}`
        });
    });

    it('should unsuccessfully remove item', async () => {
        const request = jest.spyOn(Api, 'delete')
            .mockImplementation((url) => Promise.reject(error));
        try {
            await ListService.removeItem(items[0]);
        } catch (e) {
            expect(e).toEqual('error');
        }
        expect(request).toHaveBeenCalledTimes(1);
    });
});
