import React from "react";
import {configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {store} from "../store";
import 'jsdom-global/register';
import ToDoItem from "../components/ToDoItem/ToDoItem";

configure({ adapter: new Adapter() });

const item = {
   name: 'name',
   id: 'id'
};

describe('to do item', () => {
   const wrapper = mount(
       <ToDoItem index={0} item={item} store={store}/>
   );

   it('should render correctly', () => {
       expect(wrapper).toMatchSnapshot();
   });

   it('should have id', () => {
       expect(wrapper.find('span').text()).toEqual(item.id);
   });

   it('should have name', () => {
       expect(wrapper.find('input').prop('value')).toEqual(item.name);
   });

   it('should have save button', () => {
      expect(wrapper.find('Button').first().text()).toEqual('Save');
   });

   it('should have remove button', () => {
       expect(wrapper.find('Button').last().text()).toEqual('Remove');
   });
});

