import reducer from '../reducers/reducer-list-items';
import {ADD_EMPTY_ITEM, GET_ALL_SUCCESS, ITEM_NAME_CHANGED, SAVE_SUCCESS} from "../actions/constants";

const testItem = {
    id: 'id',
    name: 'name'
};

describe('items reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual([{}])
    });

    it('should handle GET_ALL_SUCCESS', () => {

        expect(reducer(undefined, {
            type: GET_ALL_SUCCESS,
            data: [testItem]
        })).toEqual([testItem]);
    });

    it('should handle ADD_EMPTY_ITEM', () => {
        expect(reducer([], {
            type: ADD_EMPTY_ITEM
        })).toEqual([{
            name: ''
        }]);
    });

    it('should handle ITEM_NAME_CHANGED', () => {
        expect(reducer([testItem], {
            type: ITEM_NAME_CHANGED,
            payload: {
                index: 0,
                name: 'test2'
            }
        })).toEqual([{
            ...testItem,
            name: 'test2'
        }]);
    });

    it('should handle SAVE_SUCCESS', () => {
        expect(reducer([testItem], {
            type: SAVE_SUCCESS,
            data: {
                isCreate: true,
                index: 0,
                identifiers: [{
                    id: 'id2'
                }]
            }
        })).toEqual([{
            ...testItem,
            id: 'id2'
        }]);
    })
});
