import React from "react";
import {configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {Provider} from "react-redux";
import {store} from "../store";
import 'jsdom-global/register';
import ToDoList from "../components/ToToList/ToDoList";
import {act} from "@testing-library/react";

configure({ adapter: new Adapter() });

const items = [{
    name: 'name',
    id: 'id'
}];


describe('to do list', () => {
    it('should render correctly', async () => {
        let wrapper;
        await act(async () => {
            wrapper = mount(<Provider store={store}>
                <ToDoList/>
            </Provider>);
        });
        expect(wrapper).toMatchSnapshot();
    });
});
