import ListService from '../services/list.service';
import {runSaga} from 'redux-saga';
import {createOrUpdate, getAll, removeItem} from "../sagas/list-saga";
import {
    GET_ALL,
    GET_ALL_FAILURE,
    GET_ALL_SUCCESS,
    REMOVE_FAILURE,
    SAVE_FAILURE,
    SAVE_SUCCESS
} from "../actions/constants";

jest.mock('../services/list.service');

const items = [{
    id: 'id',
    name: 'name',
    index: 0
}];

describe('items saga', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should getAll successfully', async () => {
        const request = jest.spyOn(ListService, 'getList')
            .mockImplementation(() => Promise.resolve(items));
        const dispatched = [];
        await runSaga({
            dispatch: (action) => dispatched.push(action)
        }, getAll);
        expect(dispatched).toEqual([{
            type: GET_ALL_SUCCESS,
            data: items
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should getAll unsuccessfully', async () => {
        const request = jest.spyOn(ListService, 'getList')
            .mockImplementation(() => Promise.reject(GET_ALL_FAILURE));
        const dispatched = [];
        await runSaga({
            dispatch: (action) => dispatched.push(action)
        }, getAll);
        expect(dispatched).toEqual([{
            type: GET_ALL_FAILURE,
            error: GET_ALL_FAILURE
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should handle create action in createOrUpdate successfully', async () => {
        const request = jest.spyOn(ListService, 'createItem')
            .mockImplementation(() => Promise.resolve(items[0]));
        const dispatched = [];
        await runSaga({
            dispatch: (action) => dispatched.push(action)
            }, createOrUpdate, {
                payload: {
                    ...items[0],
                    id: undefined
                }
            }
        );
        expect(dispatched).toEqual([{
            type: SAVE_SUCCESS,
            data: {
                ...items[0],
                isCreate: true
            }
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should handle update action in createOrUpdate successfully', async () => {
        const request = jest.spyOn(ListService, 'updateItem')
            .mockImplementation(() => Promise.resolve(items[0]));
        const dispatched = [];
        await runSaga({
                dispatch: (action) => dispatched.push(action)
            }, createOrUpdate, {
                payload: items[0]
            }
        );
        expect(dispatched).toEqual([{
            type: SAVE_SUCCESS,
            data: {
                ...items[0],
                isCreate: false
            }
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should handle createOrUpdate unsuccessfully', async () => {
        const request = jest.spyOn(ListService, 'updateItem')
            .mockImplementation(() => Promise.reject(SAVE_FAILURE));
        const dispatched = [];
        await runSaga({
                dispatch: (action) => dispatched.push(action)
            }, createOrUpdate, {
                payload: items[0]
            }
        );
        expect(dispatched).toEqual([{
            type: SAVE_FAILURE,
            error: SAVE_FAILURE
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should handle removeItem successfully', async () => {
        const request = jest.spyOn(ListService, 'removeItem')
            .mockImplementation(() => Promise.resolve(items[0]));
        const dispatched = [];
        await runSaga({
                dispatch: (action) => dispatched.push(action)
            }, removeItem, {
                payload: items[0]
            }
        );
        expect(dispatched).toEqual([{
            type: GET_ALL,
            data: items[0]
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });

    it('should handle removeItem unsuccessfully', async () => {
        const request = jest.spyOn(ListService, 'removeItem')
            .mockImplementation(() => Promise.reject(REMOVE_FAILURE));
        const dispatched = [];
        await runSaga({
                dispatch: (action) => dispatched.push(action)
            }, removeItem, {
                payload: items[0]
            }
        );
        expect(dispatched).toEqual([{
            type: REMOVE_FAILURE,
            error: REMOVE_FAILURE
        }]);
        expect(request).toHaveBeenCalledTimes(1);
    });
});
