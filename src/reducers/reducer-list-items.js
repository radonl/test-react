import {
    ADD_EMPTY_ITEM,
    GET_ALL_SUCCESS,
    ITEM_NAME_CHANGED,
    REMOVE_ITEM, REMOVE_SUCCESS,
    SAVE_ITEM,
    SAVE_SUCCESS
} from "../actions/constants";

export default function (state = [{}], action) {
    console.log(action,state,'reducer');
    switch (action.type) {
        case GET_ALL_SUCCESS:
            state = action.data;
            break;
        case ADD_EMPTY_ITEM:
            state.push({name: ''});
            break;
        case ITEM_NAME_CHANGED:
            state[action.payload.index].name = action.payload.name;
            break;
        case SAVE_SUCCESS:
            if(action.data.isCreate) {
                state[action.data.index].id = action.data.identifiers[0].id;
            }
            break;
        default: {
            return state;
        }
    }

    return state;
}