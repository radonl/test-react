import {combineReducers} from 'redux';
import ListItemsReducer from './reducer-list-items';

const rootReducer = combineReducers({
    items: ListItemsReducer
});

export default rootReducer;