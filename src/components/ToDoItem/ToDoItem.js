import React, {useEffect} from 'react';
import {Button, Row} from "react-bootstrap";
import Col from "react-bootstrap/Col";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {itemNameChanged} from "../../actions/item-name-changed";
import {removeItem} from "../../actions/remove-item";
import {saveItem} from "../../actions/save-item";
import mainStateHook from "../../hooks/mainStateHook";
import store from "../../store";

const ToDoItem = (props) => {
    const [state, setState] = mainStateHook({name: ''});

    const init = () => {
        setState({
            ...props.item,
            index: props.index
        })
    };

    useEffect(() => {
        store.subscribe(init);
    }, []);

    useEffect(() => init(), [props.index, props.item.name]);

    return (
        <div className={"to-do-item"}>
            <Row>
                <Col xs={4}>
                    {state.id ? (<span>{state.id}</span>):false}
                </Col>
                <Col xs={4}>
                    <input type={'text'} value={state.name} onChange={(e) => props.itemNameChanged(props.index, e.target.value)}/>
                </Col>
                <Col xs={2}>
                    <Button onClick={() => props.saveItem(state)}>Save</Button>
                </Col>
                <Col xs={2}>
                    <Button variant={'danger'} onClick={() => props.removeItem(state)}>Remove</Button>
                </Col>
            </Row>
        </div>
    );
};

ToDoItem.propTypes = {
    item: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
};

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({itemNameChanged, removeItem, saveItem}, dispatch);
};


export default connect(mapStateToProps, mapDispatchToProps)(ToDoItem);
