import React, {useEffect} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from "react-bootstrap/Button";
import {Container} from "react-bootstrap";
import ToDoItem from "../ToDoItem/ToDoItem";
import mainStateHook from "../../hooks/mainStateHook";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {addEmptyItem} from "../../actions/add-empty-item";
import {loadItems} from "../../actions/load-items";
import store from "../../store";

const ToDoList = (props) => {
    const [state, setState] = mainStateHook({items: []});
    useEffect(() => {
        store.subscribe(() => {
            setState({items: props.items});
        });
        props.loadItems();
    }, []);
    return  (
        <Container className={"mt-5"}>
                     {props.items && props.items.map((item, index) => (<ToDoItem
                         item={item}
                         index={index}
                         key={`${index}-${item.id}`}
                         />))}
             <Button onClick={props.addEmptyItem}>Add</Button>
         </Container>
    );
};

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({addEmptyItem, loadItems}, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ToDoList);
