import Api from './api';

class ListService {
    async getList() {
        try {
            return await Api.get(`list`);
        } catch (e) {
            throw e.response;
        }
    }

    async createItem(item) {
        try {
            return await Api.post(`list`, {
                name: item.name
            });
        } catch (e) {
            throw e.response;
        }
    }

    async updateItem(item) {
        try {
            return await Api.put(`list`, {
                name: item.name,
                id: item.id
            });
        } catch (e) {
            throw e.response;
        }
    }

    async removeItem(item) {
        try {
            return await Api.delete(`list/${item.id}`);
        } catch (e) {
            throw e.response;
        }
    }
}

export default new ListService();