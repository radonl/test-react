import {put, takeEvery} from "@redux-saga/core/effects";
import ListService from "./../services/list.service";
import {
    GET_ALL,
    GET_ALL_FAILURE,
    GET_ALL_SUCCESS, REMOVE_FAILURE, REMOVE_ITEM,
    REMOVE_SUCCESS,
    SAVE_FAILURE,
    SAVE_ITEM,
    SAVE_SUCCESS
} from "../actions/constants";

function* getAll () {
    try {
        const data = yield ListService.getList();
        yield put({
            type: GET_ALL_SUCCESS,
            data
        });
    } catch (error) {
        yield put({
            type: GET_ALL_FAILURE,
            error
        });
    }
}

function* createOrUpdate(action) {
    try {
        const data = action.payload.id ?
            yield ListService.updateItem(action.payload):
            yield ListService.createItem(action.payload);

        yield put({
            type: SAVE_SUCCESS,
            data: {
                ...data,
                index: action.payload.index,
                isCreate: !action.payload.id
            }
        });
    } catch (error) {
        yield put({
            type: SAVE_FAILURE,
            error
        });
    }
}

function* removeItem(action) {
    try {
        const data = yield ListService.removeItem(action.payload);
        yield put({
            type: GET_ALL,
            data: action.payload
        });
    } catch (error) {
        yield put({
            type: REMOVE_FAILURE,
            error: error
        });
    }
}

export const listSaga = function*() {
    yield takeEvery(GET_ALL, getAll);
    yield takeEvery(SAVE_ITEM, createOrUpdate);
    yield takeEvery(REMOVE_ITEM, removeItem)
};

export {getAll, createOrUpdate, removeItem};