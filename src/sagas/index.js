import {listSaga} from "./list-saga";
import {all, fork} from "@redux-saga/core/effects";

function* rootSaga () {
    yield all([
        fork(listSaga),
    ]);
}

export default rootSaga;